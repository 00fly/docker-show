package com.fly.simple;

import java.io.IOException;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import com.fly.simple.utils.ShellExecutor;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class MainRun
{
    static ScheduledExecutorService service = new ScheduledThreadPoolExecutor(1);
    
    /**
     * 线程池保证程序一直运行
     * 
     * @param args
     */
    public static void main(String[] args)
    {
        service.scheduleAtFixedRate(() -> {
            try
            {
                log.info("######## {}", ShellExecutor.getDockerInfo());
            }
            catch (IOException e)
            {
                log.error(e.getMessage(), e);
            }
        }, 2, 10, TimeUnit.SECONDS);
    }
}
