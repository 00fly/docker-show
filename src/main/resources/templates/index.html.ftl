<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<style>
		body {
			margin: 10;
			font-size: 62.5%;
			line-height: 1.5;
		}

		.blue-button {
			background: #25A6E1;
			padding: 3px 20px;
			color: #fff;
			font-size: 10px;
			border-radius: 2px;
			-moz-border-radius: 2px;
			-webkit-border-radius: 4px;
			border: 1px solid #1A87B9
		}

		table {
			width: 70%;
		}

		th {
			background: SteelBlue;
			color: white;
		}

		td,
		th {
			border: 1px solid gray;
			font-size: 12px;
			text-align: center;
			padding: 5px 10px;
			overflow: hidden;
			white-space: nowrap;
			text-overflow: ellipsis;
			max-width: 200px;
			white-space: nowrap;
			text-overflow: ellipsis;
			text-overflow: ellipsis;
		}
	</style>
</head>
<title>Welcome to nginx!</title>
<body>
	<center>
		<table>
			<tr>
				<th>No.</th>
				<th>Application</th>
				<th>Ports</th>
			</tr>
			<#list map?keys as key>
			<tr>
				<td>${key_index+1}</td>
				<td style="text-align:left">${key}</td>
				<td>
				<#list map[key] as port>
					<a target="_blank" href="http://${ip}:${port}">${port}</a>&nbsp;&nbsp;&nbsp;&nbsp;
				</#list>
				</td>
			</tr>
			</#list>
		</table>
		<h3 style="color:red;">更新时间：${date?string('yyyy-MM-dd HH:mm:ss')}</h3>
	</center>
</body>

</html>