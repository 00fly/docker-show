# 一、需求概况
现有某云主机服务器，用来做项目演示用，上面运行了docker应用，现希望有一总览页面，用来展示部署的应用。
# 二、业务流程

```mermaid
graph LR
A(获取docker信息)--> B(模板生成页面)--> C(挂载到nginx) 

```
/usr/share/nginx/html/index.html 

任务调度采用`crontab`

演示地址：[http://124.71.129.204](http://124.71.129.204/)






# 三、Java工程打包

## 1. 一体化可执行包

配置文件：pom-deps.xml

## 2. 带外部依赖lib的可执行包

配置文件：pom-lib.xml


## 3. 执行命令
```shell
#完整打包
mvn clean package

#一体化可执行包
mvn clean package -f pom-deps.xml

#带外部依赖lib的可执行包
mvn clean package -f pom-lib.xml
```